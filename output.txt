


#include <stdio.h>
#include <stdlib.h>
#include <string.h>



struct entry {
	char b;
	struct entry *next;
} *head, *new_character;


int n = -1;


void stack_push(char c) {
	
	new_character = malloc(sizeof(struct entry));

	
	n += 1;

	
	
    new_character -> b = c;
	new_character -> next = head;
	head = new_character;
}
