/*
 * This programm takes command line arguements of an input C source file and an output file. The input file will be
 * parsed and have it's commenting removed and brackets checked.
 *
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>


// Creating the struct to hold the pushed characters.
struct entry {
	char b;
	struct entry *next;
} *head, *new_character;

// Creating the variable to establish whether linked lists are empty.
int n = -1;

// push a character into the stack
void stack_push(char c) {
	//dynamically allocating memory the size of struct entry to new character.
	new_character = malloc(sizeof(struct entry));

	//incrementing the n count, to show linked list is not empty.
	n += 1;

	//adding the new input to the head of the list and pushing back the current
	// entries.
    new_character -> b = c;
	new_character -> next = head;
	head = new_character;
}
