# C code comment remover and brackets checker

My implementation of a piece of software that can be used to remove all
comments and check bracket format in C source code. This programm makes use
of the stack in the form of a linked list which can be pushed and popped,
creating dynamically allocated memory with malloc.


# Why I made this programm

This programm was one of my first projects at university, during my introduction
to the C programming language.


# Features

    * Takes 2 '.txt' files as command line arguments. The first file should be
        your input source code, the second file is the output file where the
        finished product will be written.
    
    * If there are any brackets that are not matched. The programm will flag
        an error and give a line number.
        

# How to run

Collect all 3 files in the same directory.

Through a linux command line:

    * $ gcc -o main main.c
    * $ ./main input.txt output.txt
    

# Working examples

C input source code with no errors:

![input](/images/input-normal.png)


Output txt file:

![output](/images/output.png)


Missing bracket input and command line response:

![brackets](/images/wrong-bracket.png)


# Specification

[Link: Project Specification](https://gitlab.com/lawebb/c-code-comment-remover-and-brackets-checker/-/tree/master/specification/COMP1711_Assignment_3.pdf)