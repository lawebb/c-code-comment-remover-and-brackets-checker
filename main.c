/*
 * This programm takes command line arguements of an input C source file and an output file. The input file will be
 * parsed and have it's commenting removed and brackets checked.
 *
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>


// Creating the struct to hold the pushed characters.
struct entry {
	char b;
	struct entry *next;
} *head, *new_character;

// Creating the variable to establish whether linked lists are empty.
int n = -1;

// push a character into the stack
void stack_push(char c) {
	//dynamically allocating memory the size of struct entry to new character.
	new_character = malloc(sizeof(struct entry));

	//incrementing the n count, to show linked list is not empty.
	n += 1;

	//adding the new input to the head of the list and pushing back the current
	// entries.
    new_character -> b = c;
	new_character -> next = head;
	head = new_character;
}

/* Overwrites the character pointed by c with the character currently on
 * top of the stack.
 *
 * The function returns 0 if the operation can be carried out successfully,
 * and any non-zero number if the operation could not be performed (for instance
 * because the stack is empty).
 */
int stack_top(char *c) {
	//if the list is empty, return true.
	if (n == -1) {
		return 1;
	}
	// if the list is not empty, copy the character at the top of the stack to
	// the given pointer.
	else {
		*c = head -> b;
		return 0;
	}
};


/* Overwrites the character pointed by c with the character currently on
 * top of the stack, and removes the character from the top of the stack.
 *
 * The function returns 0 if the operation can be carried out successfully,
 * and any non-zero number if the operation could not be performed (for instance
 * because the stack is empty).
 */
int stack_pop(char *c) {
	// if the list is empty, return true.
	if (n == -1) {
		return 1;
	}
	// if the list is not empty, decrement the linked list counter, so it will be
	// updated with how many elements are remaining, then copy the character at
	// the top of the list to the given pointer.
	else {
		n -= 1;

		*c = head -> b;

		// create a struct to hold the head of the list, to pop the current head
		// from the list.
		struct entry *temp;
		temp = head;

		head = temp -> next;
		free(temp);
		return 0;
	}
};

/* Returns a dynamically allocated string (in the heap)
 * containing the characters in the string pointed by source,
 * excluding any one-line comment.
 */
char *remove_single_line_comment(const char *source){
	//dynamically allocating space to a string to hold the code when comments
	// are removed.
	char *s = (char*) malloc((strlen(source) + 1) * (sizeof(char)));

	// com will be used to determine whether the code is in a state of comments
	// or not, numchar will be used to count the number of characters added to
	// the new string and increment the array.
	int com = 0, numchar = 0;

	// itterating over each character of the list
	for(int i = 0; source[i] != '\0'; i++) {
		// if comments are detected, then comment state it set to true.
		if(source[i] == '/' && source[i + 1] == '/'){
			com = 1;
		}
		// once a new line is detected, comment state is set to false.
		if(source[i] == '\n') {
			com = 0;
		}
		// if comment state is false, then the characters are copied into the new
		// string and numchar incremented.
		if(com == 0){
			s[numchar] = source[i];
			numchar++;
		}
	}
	s[numchar] = '\0';
	return s;
};

/* Returns a dynamically allocated string (in the heap)
 * containing the characters in the string pointed by source,
 * excluding any multi-line comment.
 */
char *remove_multi_line_comment(const char *source) {
	//dynamically allocating space to a string to hold the code when comments
	// are removed.
	char *t = (char*) malloc((strlen(source) + 1) * (sizeof(char)));

	// com will be used to determine whether the code is in a state of comments
	// or not, numchar will be used to count the number of characters added to
	// the new string and increment the array.
	int com = 0, numchar = 0;

	// itterating over each character of the list
	for(int i = 0; source[i] != '\0'; i++) {
		// if comments are detected, then comment state it set to true.
		if(source[i] == '/' && source[i + 1] == '*'){
			com = 1;
		}
		// once a new line is detected, comment state is set to false. If comments
		// were originally true, then the counter is incremented, so that the end
		// of comment tags are not printed to the new string.
		if(source[i] == '*' && source[i + 1] == '/') {
			if(com == 1){
				i = i + 2;
			}
			com = 0;
		}
		// if comment state is false, then the character is copied to the new string.
		if(com == 0){
			t[numchar] = source[i];
			numchar++;
		}
	}
	t[numchar] = '\0';
	return t;
};

/* The main function of your program. Rename into mymain
 * for testing.
 */
int main(int argc, char **argv) {

	char *pp,a;
	a = 'a';
	pp = &a;

	//Initialising a variable for line counter and array counter.
	int line = 1, ln = 0;
	//initialising an array to hold the line number of the pushed characters
	int ob_line[1000];

	//opening the files from the argument and setting the input to read and the
	// output to write.
	FILE *input = fopen (argv[1], "r");
	FILE *output = fopen (argv[2], "w");

	//Calculating the length of the input string by using fseek and ftell function
	//before resetting the seek position.
	fseek(input, 0, SEEK_END);
	int filelen = ftell (input);
	fseek(input, 0, SEEK_SET);

	//Dynamically allocating memory to a new string that will hold the input
	// string, so that it can be run through the functions.
	char *v = (char*) malloc((filelen + 1) * (sizeof(char)));

	// copying the characters from the input into the new string.
	for (int y = getc(input), i = 0; y != EOF; y = getc(input), i++) {
		v[i] = y;
	}

	//Performing the functions to remove comments on v, so the input now has no
	//comments.
	v = remove_single_line_comment(v);
	v = remove_multi_line_comment(v);

	// Printing the input (without comments) to the output file.
	for(int i = 0; v[i] != 0; i++) {
		fprintf(output, "%c", v[i]);
	}

	//resetting the pointer in input to read characters from the beginning again.
	fseek(input, 0, SEEK_SET);

	for (int x = getc(input); x != EOF; x = getc(input)) {
		if (x == '{' || x == '}' || x == '(' || x == ')' || x == '[' || x == ']') {
			//If an opening bracket is inputted, then it is pushed in to the stack,
			// the line number is stored in the array and array counter is incremented.
			if (x == '{' || x == '(' || x == '[') {
				stack_push(x);
				ob_line[ln] = line;
				ln += 1;
			}
			// If a closing bracket is added while there are no characters in the stack,
			// a type 1 error message is printed, alongside the line number of the error.
			if ((x == '}' && n == -1) || (x == ')' && n == -1) || (x == ']' && n == -1)){
				printf("Type 1 Error Occured at Line %i\n", line);
				return 0;
			}
			//if there is a closing bracket encountered, without a matching opening
			// bracket at the head of the stack, then a type 2 error message is
			// printed alongside the line number of the error.
			else if ((head -> b != '{' && x == '}') || (head -> b != '(' && x == ')') || (head -> b != '[' && x == ']')){
				printf("Type 2 Error Occured at Line %i\n", line);
				return 0;
			}
			// If there is a closing bracket encountered, while there is a matching
			// opening bracket in the stack, then the opening bracket is popped from
			// the stack, and the line number is removed from its position in the array
			// and the array is decremented.
			else if ((head -> b == '{' && x == '}') || (head -> b == '(' && x == ')') || (head -> b == '[' && x == ']')){
				stack_pop(pp);
				ln -= 1;
				ob_line[ln] = 0;
			}
		}

		//when a new line is encountered, the line counter is incremented.
		if (x == '\n') {
			line += 1;
		}
	}

	//if there exists an opening bracket which has not been closed, the first
	// case of which this happens will be used and a type 3 error will be printed
	//alongside the line number of the error.
	if(ob_line[0] != 0){
		printf("Type 3 Error Occured at Line %i\n", ob_line[0]);
		printf("%i", ob_line[1]);
	}

	//the input and output files are closed before ending the program.
	fclose(input);
	fclose(output);

	return 0;
}
